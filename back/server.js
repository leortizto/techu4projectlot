var express = require("express");
var app = express();
var port = process.env.PORT || 3000;
var bodyParser = require('body-parser');
app.use(bodyParser.json());
var requestJson = require('request-json');

var baseMlabURL = "https://api.mlab.com/api/1/databases/apitechu/collections/";
var mlabAPIKey = "apiKey=l-3CuGaX_bvhn0XaqvACGc_B9ccb8P1V";

// Escuchamos en el puerto
app.listen(port);
console.log("API escuchando en el puerto " + port);


//Obtiene todos los usuarios dados de alta dentro de la colección MongoDB usuarios
app.get('/usuarios',
   function(req, res)
   {
     console.log("GET /usuarios");
     httpClient = requestJson.createClient(baseMlabURL);
     console.log("cliente creado");

     httpClient.get("usuarios?" + mlabAPIKey,
        function(err, resMLab, body) {
          var response = !err ? body : {
               "msg":"Error obteniendo usuarios"
          }
          res.send(response);
        }
       )
   }
);

//Devuelve un Usuario, pasado por id en la cabecera, de la colección MongoDB usuarios
app.get('/usuarios/:id',
 function(req, res) {

   console.log("GET /usuarios/:id");

   var idUsuario = req.params.id;
   var query = 'q={"idUsuario" : ' + idUsuario + '}';

   httpClient = requestJson.createClient(baseMlabURL);

   console.log("Usuario pedido");

   httpClient.get("usuarios?" + query + "&" + mlabAPIKey,
     function(err, resMLab, body) {
       if (err) {
         response = {
           "msg" : "Error obteniendo usuario."
         }
         res.status(500);
       } else {
         if (body.length > 0) {
           response = body[0];
         } else {
           response = {
             "msg" : "Usuario no encontrado."
           };
           res.status(404);
         }
       }
       res.send(response);
     }
   )
 }
);

//Da de alta un usuario, en la colección MongoDB usuarios,
//Los datos del usuario se pasan en el body con formato JSON
app.post('/usuario',
  function(req, res) {

    console.log("POST /usuario");

    var query = 's={"idUsuario": -1}'

    console.log ("usuarios?" + query + "&" + mlabAPIKey);
    httpClient = requestJson.createClient(baseMlabURL);

    httpClient.get("usuarios?" + query + "&" + mlabAPIKey,
      function(err, resMLab, body) {
          console.log("Ultimo usuario:" + body)
          if (body.length > 0) {
            console.log ("Obtenemos ultimo idUsuario: " + body[0].idUsuario);
            var idUsuario = parseInt(1) + parseInt(body[0].idUsuario);
            console.log ("idUsuario a intertar: " + idUsuario);
            var usuario = {
                "idUsuario" : idUsuario,
                "email"     : req.body.email,
                "password"  : req.body.password,
                "lastName"  : req.body.lastName,
                "firstName" : req.body.firstName,
             }
            httpClient.post("usuarios?" + "&" + mlabAPIKey, usuario,
              function(errPUT, resMLabPUT, bodyPOST) {
                console.log("POST realizado");
                var response = {
                "msg" : "Usuario dado de alta",
                }
                res.send(response);
              }
            );
          } else {
            response = {
              "msg" : "Id no encontrado"
            };
            res.send(response);
          }
        }
    );
  }
);

//Devuelve todas las cuentas que tiene dadas de alta un usuario de la colección MongoDB cuentas,
//El usuario del que queremos obtener las cuenta se pasa por id en la cabecera
app.get('/usuarios/:id/cuentas',
 function(req, res) {

   console.log("GET /usuarios/:id/cuentas");

   var idUsuario = req.params.id;
   var query = 'q={"idUsuario" : ' + idUsuario + '}';
   httpClient = requestJson.createClient(baseMlabURL);

   console.log("Cuenta pedida");

   httpClient.get("cuentas?" + query + "&" + mlabAPIKey,
     function(err, resMLab, body) {
       if (err) {
         response = {
           "msg" : "Error obteniendo cuenta."
         }
         res.status(500);
       } else {
         if (body.length > 0) {
           response = body;
         } else {
           response = {
             "msg" : "Cuenta no encontrada."
           };
           res.status(404);
         }
       }
       res.send(response);
     }
   )
 }
);

//Devuelve todas los movimientos que tiene dados de alta una cuenta de la colección MongoDB movimientos,
//La cuenta de la que queremos obtener los movimientos se pasa por id en la cabecera
app.get('/cuenta/:id/movimientos/',
 function(req, res) {

   console.log("GET /cuenta/:id/movimientos/");

   var idCuenta = req.params.id;
   var query = 'q={"idCuenta" : ' + idCuenta + '}';
   httpClient = requestJson.createClient(baseMlabURL);

   console.log("Movimientos pedidos");

   httpClient.get("movimientos?" + query + "&" + mlabAPIKey,
     function(err, resMLab, body) {
       if (err) {
         response = {
           "msg" : "Error obteniendo movimientos."
         }
         res.status(500);
       } else {
         if (body.length > 0) {
           response = body;
         } else {
           response = {
             "msg" : "Movimientos no encontrados."
           };
           res.status(404);
         }
       }
       res.send(response);
     }
   )
 }
);

//Da de alta un movimiento a una cuenta en la colección MongoDB movimientos
//Actualiza el balance en la colección MongoDB cuentas con el importe del movimiento tanto en positivo como negativo
//La cuenta a la que queremos insertar el movimiento se pasa por id en la cabecera
//Los datos del movimiento a dar de alta se pasan en el body con formato JSON
app.post('/cuenta/:id/movimiento/',
  function(req, res) {

    console.log("POST /cuenta/:id/movimiento/");

    var query = 's={"idMovimiento": -1}'

    console.log ("movimientos?" + query + "&" + mlabAPIKey);
    httpClient = requestJson.createClient(baseMlabURL);

    httpClient.get("movimientos?" + query + "&" + mlabAPIKey,
      function(err, resMLab, body) {
          if (body.length > 0) {
            console.log ("Obtenemos ultimo idMovimiento: " + body[0].idMovimiento);
            var idMovimiento = parseInt(1) + parseInt(body[0].idMovimiento);
            var idCuenta = parseInt(req.params.id);
            var dt = new Date();
            var month = dt.getMonth()+1;
            var day = dt.getDate();
            var year = dt.getFullYear();
            var hora = new Date()
            var horas = hora.getHours()
            var minutos = hora.getMinutes()
            var horaMovimiento = horas + ':' + minutos;
            var fechaMovimiento = month + '/' + day + '/' + year;
            console.log ("idMovimiento a intertar: " + idMovimiento);
            var nuevomovimiento = {
                "idMovimiento"   : idMovimiento,
                "importe"        : req.body.importe,
                "fechaMovimiento": fechaMovimiento,
                "horaMovimiento" : horaMovimiento,
                "desMovimiento"  : req.body.desMovimiento,
                "idCuenta"       : idCuenta
             }
            httpClient.post("movimientos?" + "&" + mlabAPIKey, nuevomovimiento,
              function(errPUT, resMLabPUT, bodyPOST) {
                console.log("POST realizado alta movimiento");
                //var response = {
                //"msg" : "Movimiento dado de alta",
                //}
                //res.send(response);
                console.log("PUT actualizamos balance idCuenta: " + idCuenta);
                var query2 = 'q={"idCuenta": ' + idCuenta + '}';
                console.log("query2: " + query2);
                httpClient.get("cuentas?" + query2 + "&" + mlabAPIKey,
                  function(err2, resMLab2, body2){
                    var encontrado = false;
                    if (err2) {
                      response = {
                        "msg" : "Error obteniendo cuenta."
                      };
                      res.send(response);
                    }
                    else {
                      if (body.length > 0) {
                        console.log("Cuenta encontrada.");
                        console.log("respuesta:" + body2[0]);
                        encontrado=true;
                        if (encontrado){
                          console.log("Cuenta encontrada. Actualizamos balance");
                          var query3 = 'q={"idCuenta": ' + body2[0].idCuenta + '}';
                          console.log("query3: " + query3);
                          console.log("balance: " + body2[0].balance);
                          var newbalance = (parseInt(body2[0].balance) + parseInt(req.body.importe));
                          console.log("newbalance: " + newbalance);

                          var putBody = '{"$set":{"balance":' + newbalance + '}}';
                          console.log("putBody: " + putBody);

                          httpClient.put("cuentas?" + query3 + "&" + mlabAPIKey, JSON.parse(putBody),
                            function(errPut, resMLabPut, bodyPut){
                              console.log("cuentas?" + query3 + "&" + mlabAPIKey );
                              if (errPut) {
                                response = {
                                  "msg" : "Error en actualizacion balance."
                                };
                                res.send(response);
                              }
                              else {
                                response = {
                                  "msg" : "Movimiento dado de alta y actualizacion balance OK"
                                };
                                res.send(response);
                              }
                            }
                          );
                        }else {
                          response = {
                            "msg" : "Cuenta no encontrada."
                          };
                          res.send(response);
                        }
                      }
                    }
                  }
                );
              }
            );
          }else {
            response = {
              "msg" : "Id no encontrado"
            };
            res.send(response);
            }
        }
    );
  }
);

//Da de alta una cuenta en la colección MongoDB cuentas
//Si la cuenta ha sido dada de alta con balance > 0 entonces ademas da alta un movimiento en la
//coleción MongoDB movimientos
//Los datos necesarios para dar de alta la cuenta y el movimiento en caso de baance > 0 se
//pasan en el body por JSON
app.post('/usuario/cuenta',
  function(req,res){

    console.log("POST /usuario/cuenta");

    var query = 's={"idCuenta": -1}'

    console.log ("cuentas?" + query + "&" + mlabAPIKey);
    httpClient = requestJson.createClient(baseMlabURL);

    httpClient.get("cuentas?" + query + "&"+   mlabAPIKey,
      function(err, resMLab, body){
        if (body.length > 0) {
          console.log ("Obtenemos ultimo idCuenta: " + body[0].idCuenta);
          var idCuenta = parseInt(1) + parseInt(body[0].idCuenta);
          var numAle = getRandomInt(1000000000000000,9999999999999999);
          var iban = "ES98 0182 "+ numAle;
          console.log ("idCuenta a intertar: " + idCuenta);
          console.log ("balance:" + req.body.balance)
          var nuevacuenta = {
              "idCuenta"       : idCuenta,
              "iban"           : iban,
              "balance"        : req.body.balance,
              "desCuenta"      : req.body.desCuenta,
              "idUsuario"      : req.body.idUsuario
           }
          httpClient.post("cuentas?" + "&" + mlabAPIKey, nuevacuenta,
            function(errPUT, resMLabPUT, bodyPOST) {
              console.log("POST realizado");
              console.log(idCuenta);
              if ((req.body.balance)> 0){
                var query = 's={"idMovimiento": -1}'

                console.log ("movimientos?" + query + "&" + mlabAPIKey);
                httpClient = requestJson.createClient(baseMlabURL);

                httpClient.get("movimientos?" + query + "&" + mlabAPIKey,
                  function(err, resMLab, body) {
                      if (body.length > 0) {
                        console.log ("Obtenemos ultimo idMovimiento: " + body[0].idMovimiento);
                        console.log(idCuenta);
                        var idMovimiento = parseInt(1) + parseInt(body[0].idMovimiento);
                        var dt = new Date();
                        var month = dt.getMonth()+1;
                        var day = dt.getDate();
                        var year = dt.getFullYear();
                        var hora = new Date()
                        var horas = hora.getHours()
                        var minutos = hora.getMinutes()
                        var horaMovimiento = horas + ':' + minutos;
                        var fechaMovimiento = month + '/' + day + '/' + year;
                        console.log ("idMovimiento a intertar: " + idMovimiento);
                        var nuevomovimiento = {
                            "idMovimiento"   : idMovimiento,
                            "importe"        : req.body.balance,
                            "fechaMovimiento": fechaMovimiento,
                            "horaMovimiento" : horaMovimiento,
                            "desMovimiento"  : "Ingreso nueva cuenta",
                            "idCuenta"       : idCuenta
                         }
                        httpClient.post("movimientos?" + "&" + mlabAPIKey, nuevomovimiento,
                          function(errPUT, resMLabPUT, bodyPOST) {
                            console.log("POST realizado alta movimiento");
                            if (errPUT) {
                              response = {
                                "msg" : "Error en actualizacion movimiento."
                              };
                              res.send(response);
                            }
                            else {
                              response = {
                                "msg" : "Cuenta dada de alta y actualizacion movimiento"
                              };
                              res.send(response);
                            }
                          }
                        );
                      }
                    }
                  );

              }else{
                  response = {
                    "msg" : "Cuenta dada de alta"
                  }
                  res.send(response);
              };
            }
          );
        } else {
          response = {
            "msg" : "Id no encontrado"
           }
          res.send(response);
          };
      }
    );
  }
);


function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}

//HACER LOGIN USUARIO
//Pone logged=true, en el registro que corresponde al usuario y password pasado
//por el body dentro de la colección de MondoDB usuarios
app.post('/login',
 function(req, res) {

   console.log("POST /login");

   var email = req.body.email;
   var password = req.body.password;
   var query = 'q={"email": "' + email + '", "password":"' + password +'"}';

   console.log("query es: " + query);

   httpClient = requestJson.createClient(baseMlabURL);

   httpClient.get("usuarios?" + query + "&" + mlabAPIKey,
     function(err, resMLab, body) {
       if (body.length == 0) {
         var response = {
           "mensaje" : "Login incorrecto, email y/o passsword no encontrados"
         }
         res.send(response);
       } else {
         console.log("Actualiza estado");
         query = 'q={"idUsuario" : ' + body[0].idUsuario +'}';
         console.log("Query for put is " + query);
         var putBody = '{"$set":{"logged":true}}';
         httpClient.put("usuarios?" + query + "&" + mlabAPIKey, JSON.parse(putBody),
           function(errPUT, resMLabPUT, bodyPUT) {
             console.log("PUT realizado");
             var response = {
               "msg" : "Usuario logado con éxito",
               "Usuario" : body[0]
             }
             res.send(response);
           }
         );
       }
     }
   );
 }
);

//HACER LOGOUT USUARIO
//Quita logged=true, en el registro que corresponde al usuario y password pasado
//por el body dentro de la colección de MondoDB usuarios
app.post('/logout',
 function(req, res) {

   console.log("POST /logout");
   var idUsuario = req.body.idUsuario;

   var query = 'q={"idUsuario": ' + idUsuario + '}';

   console.log("query es " + query);

   httpClient = requestJson.createClient(baseMlabURL);

   httpClient.get("usuarios?" + query + "&" + mlabAPIKey,
     function(err, resMLab, body) {
       if (body.length == 0) {
         var response = {
           "mensaje" : "Logout incorrecto, usuario no encontrado"
         }
         res.send(response);
       } else {
         console.log("Actualiza estado");
         query = 'q={"idUsuario" : ' + body[0].idUsuario +'}';
         console.log("Query for put is " + query);
         var putBody = '{"$unset":{"logged":""}}'
         httpClient.put("usuarios?" + query + "&" + mlabAPIKey, JSON.parse(putBody),
           function(errPUT, resMLabPUT, bodyPUT) {
             console.log("PUT realizado");
             var response = {
               "msg" : "Usuario deslogado con éxito",
               "idUsuario" : body[0].idUsuario
             }
             res.send(response);
           }
         );
       }
     }
   );
 }
);
